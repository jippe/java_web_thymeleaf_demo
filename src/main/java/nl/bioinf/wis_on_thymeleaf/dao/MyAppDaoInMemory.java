package nl.bioinf.wis_on_thymeleaf.dao;

import nl.bioinf.wis_on_thymeleaf.model.Role;
import nl.bioinf.wis_on_thymeleaf.model.User;

import java.util.HashMap;
import java.util.Map;

public class MyAppDaoInMemory implements MyAppDao {
    private Map<String, User> userDb = new HashMap<>();

    public MyAppDaoInMemory() {
        createDb();
    }

    @Override
    public void connect() throws DatabaseException {
        //pass silently
    }

    @Override
    public User getUser(String userName, String userPass) throws DatabaseException {
        if (this.userDb.containsKey(userName)) return this.userDb.get(userName);
        return null;
    }

    @Override
    public void insertUser(String userName, String userPass, String email, Role role) throws DatabaseException {
        this.userDb.put(userName, new User(userName, userPass, email, role));
    }

    @Override
    public void disconnect() throws DatabaseException {
        //pass silently
    }

    private void createDb() {
        userDb.put("Henk",
                new User("Henk", "henk@internal.com", "henkieissafe", Role.USER));
        userDb.put("Floor",
                new User("Floor", "floortje", "floor@example.com", Role.ADMIN));
        userDb.put("Gijs",
                new User("Gijs", "gijssie", "gijs@example.com", Role.ADMIN));

    }

}

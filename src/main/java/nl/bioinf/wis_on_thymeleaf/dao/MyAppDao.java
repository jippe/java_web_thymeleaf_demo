package nl.bioinf.wis_on_thymeleaf.dao;


import nl.bioinf.wis_on_thymeleaf.model.Role;
import nl.bioinf.wis_on_thymeleaf.model.User;

public interface MyAppDao{

    /**
     * connection logic should be put here
     * @throws DatabaseException
     */
    void connect() throws DatabaseException;

    /**
     * shutdown logic should be put here
     * @throws DatabaseException
     */
    void disconnect() throws DatabaseException;
    /**
     * fetches a user by username and password.
     * @param userName
     * @param userPass
     * @return
     * @throws DatabaseException
     */
    User getUser(String userName, String userPass) throws DatabaseException;

    /**
     * inserts a new User.
     * @param userName
     * @param userPass
     * @param email
     * @param role
     * @throws DatabaseException
     */
    void insertUser(String userName, String userPass, String email, Role role) throws DatabaseException;

}
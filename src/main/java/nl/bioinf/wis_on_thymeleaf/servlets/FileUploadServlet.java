package nl.bioinf.wis_on_thymeleaf.servlets;

import nl.bioinf.wis_on_thymeleaf.config.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.http.Part;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

//moved to web.xml
//@MultipartConfig(location="/tmp",
//        fileSizeThreshold = 1024 * 1024,
//        maxFileSize = 1024 * 1024 * 5,
//        maxRequestSize = 1024 * 1024 * 5 * 5)
@WebServlet(name = "FileUploadServlet", urlPatterns = "/data_upload")
public class FileUploadServlet extends HttpServlet {

    private TemplateEngine templateEngine;
    private String uploadDir;

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.getTemplateEngine();
        this.uploadDir = getInitParameter("upload_dir");

        //or, use relative to this app:
        // gets absolute path of the web application
        String applicationPath = getServletContext().getRealPath("");
        String uploadFilePath = applicationPath + File.separator + uploadDir;
        System.out.println("uploadFilePath = " + uploadFilePath);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("upload_form", ctx, response.getWriter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        File fileSaveDir = new File(this.uploadDir);
        if (! fileSaveDir.exists()) {
            throw new IllegalStateException("Upload dir does not exist: " + this.uploadDir);
        }

        //Do this only if you are sure there won't be any file name conflicts!
        //An existing one will simply be overwritten
//        String fileName;
//        for (Part part : request.getParts()) {
//            fileName = part.getSubmittedFileName();
//            part.write(this.uploadDir + File.separator + fileName);
//        }

        //the safe way, with a generated file name becomes something like this
        //my_app_upload14260971264207930189.csv
        File generatedFile = File.createTempFile("my_app_upload", ".csv");
        for (Part part : request.getParts()) {
            part.write(this.uploadDir + File.separator + generatedFile.getName());
        }

        //go back to the upload form
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("message", "upload successfull, wanna do another on?");
        templateEngine.process("upload_form", ctx, response.getWriter());
    }

}

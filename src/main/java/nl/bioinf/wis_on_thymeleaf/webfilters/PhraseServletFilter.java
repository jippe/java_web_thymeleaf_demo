package nl.bioinf.wis_on_thymeleaf.webfilters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns = "/give.phrase")
public class PhraseServletFilter implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        /*not interesting here*/
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            System.out.println("Request for " + ((HttpServletRequest)request).getRequestURL().toString());
            System.out.println("Query: " + ((HttpServletRequest)request).getQueryString());
            System.out.println("Remote address: " + request.getRemoteAddr());
        }
        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        /*not interesting here*/
    }
}


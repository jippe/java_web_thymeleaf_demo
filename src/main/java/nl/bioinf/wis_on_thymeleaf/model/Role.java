package nl.bioinf.wis_on_thymeleaf.model;

public enum Role {
    GUEST,
    USER,
    ADMIN;
}

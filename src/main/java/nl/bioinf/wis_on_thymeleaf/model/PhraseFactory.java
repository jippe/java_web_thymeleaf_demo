package nl.bioinf.wis_on_thymeleaf.model;

import java.util.Random;

public class PhraseFactory {
    public static int MAX_PHRASE_COUNT = 4;
    public static String getPhrase(String phraseType) {
        //I only have 4 phrases of each category
        Random rand = new Random();
        int phraseIndex = rand.nextInt(MAX_PHRASE_COUNT) + 1;
        return Integer.toString(phraseIndex);
    }
}
